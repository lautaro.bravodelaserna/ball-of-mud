using System.Collections.Generic;

public class BuquebusPresenter
{
    private readonly IBuquebusView _view;
    private readonly IShipIdRepository _shipIdRepository;
    private readonly IPassengersAvailableRepository _passengersAvailableRepository;

    private List<string> _services = new List<string>();
    private int _indexOfTicketBuyed;

    private string zone1 = "Bar";
    private string zone2 = "VIP";
    private string zone3 = "Game Zone";

    public BuquebusPresenter(IBuquebusView view, IShipIdRepository shipIdRepository,
        IPassengersAvailableRepository passengersAvailableRepository)
    {
        _view = view;
        _shipIdRepository = shipIdRepository;
        _passengersAvailableRepository = passengersAvailableRepository;
    }

    public void SetUpZones()
    {
        _services.Add(zone1);
        _services.Add(zone2);
        _services.Add(zone3);
    }

    public void Initialize()
    {
        var shipId = _shipIdRepository.Get();
        var passengersAvailable = _passengersAvailableRepository.Get();

        _view.ShowTicketsAvailable(shipId, passengersAvailable, _services);
    }

    public void BuyIndexShip(int indexOfTicketToBuy)
    {
        _indexOfTicketBuyed = indexOfTicketToBuy;
    }

    public void DestroyedView()
    {
        var passengers = _passengersAvailableRepository.Get()[_indexOfTicketBuyed];
        _passengersAvailableRepository.Set(_indexOfTicketBuyed, passengers - 1);
    }
}