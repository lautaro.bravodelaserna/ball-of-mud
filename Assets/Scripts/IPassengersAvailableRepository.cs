using System.Collections.Generic;

public interface IPassengersAvailableRepository
{
    int[] Get();
    void Set(int index, int passengers);
}

public class PassengersAvailableRepository : IPassengersAvailableRepository
{
    private List<int> _passengers = new List<int>
    {
        200,
        400,
        100,
        150
    };

    public int[] Get()
    {
        return _passengers.ToArray();
    }

    public void Set(int index, int passengers)
    {
        _passengers[index] = passengers;
    }
}