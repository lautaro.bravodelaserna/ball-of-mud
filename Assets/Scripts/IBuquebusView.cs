using System;
using System.Collections.Generic;
using UnityEngine;

public interface IBuquebusView
{
    void ShowTicketsAvailable(string[] shipIds, int[] passengersAvailable, List<string> services);
}

public class BuquebusView : MonoBehaviour, IBuquebusView
{
    private BuquebusPresenter _presenter;

    private void Awake()
    {
        IShipIdRepository shipIdRepository = new ShipIdRepository();
        IPassengersAvailableRepository passengersRepository = new PassengersAvailableRepository();
        _presenter = new BuquebusPresenter(this, shipIdRepository, passengersRepository);
        
        _presenter.SetUpZones();
    }

    private void Start()
    {
        _presenter.Initialize();
        _presenter.BuyIndexShip(2);
    }

    private void OnDestroy()
    {
        _presenter.DestroyedView();
    }

    public void ShowTicketsAvailable(string[] shipIds, int[] passengersAvailable, List<string> services)
    {
        foreach (var shipId in shipIds)
        {
            Debug.Log("Ship id" + shipId);
        }
        
        foreach (var passengers in passengersAvailable)
        {
            Debug.Log("Passengers" + passengers);
        }

        foreach (var service in services)
        {
            Debug.Log("Service:" + service);
        }
    }
}
