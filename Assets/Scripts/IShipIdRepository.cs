public interface IShipIdRepository
{
    string[] Get();
}

public class ShipIdRepository : IShipIdRepository
{
    public string[] Get()
    {
        return new[]
        {
            "BSAS-MON001",
            "BSAS-MON002",
            "BSAS-COL001",
            "BSAS-COL002",
        };
    }
}